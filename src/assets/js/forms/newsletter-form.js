import $ from 'jquery';
import validate from 'jquery-validation';
import 'jquery-validation/dist/additional-methods';
import rb from '../rb';

// Foundation JS relies on a global varaible. In ES6, all imports are hoisted
// to the top of the file so if we used `import` to import Foundation,
// it would execute earlier than we have assigned the global variable.
// This is why we have to use CommonJS require() here since it doesn't
// have the hoisting behavior.
window.jQuery = $;

$(document).ready(function () {

	// Disable the default submit method
	$('form.newsletter-signup__form').submit(function (e) {
		e.preventDefault();
	});

	$("form.newsletter-signup__form").validate({
		errorClass: "error",
		errorElement: "span",
		errorPlacement: function (error, element) {
			console.log ( "Placing error" );
			$(error).text();
			if (element.attr('type') === 'checkbox') {
				$(element).siblings(".error").text($(error).text());
			} else {
				// Append error within associated label
				$(element)
					.closest("form")
						.find("label[for='" + element.attr("name") + "']")
						.find(".newsletter-signup__labeltext")
							.after(error);
				// And make the label text visible
				// $( element )
				// 	.closest( "form" )
				// 		.find( "label[for='" + element.attr( "name" ) + "']" )
				// 			.find('span')
				// 				.css("visibility", "visible");
			}

		},
		submitHandler: function (form) {
			console.log("submitHandler");
			// Submit the form to MailChimp
			// TODO - correctly scope this
			let inputs = $(form).find('input');

			var values = {};
			inputs.each(function () {
				values[this.name] = $(this).val();
			});

			let mailchimp = {
				email_address: values.nl_email,
				status: 'subscribed',
				merge_fields: {
					FNAME: values.nl_firstname,
					LNAME: values.nl_lastname
				}
			};
			console.log("Thanks for subscribing!");
			console.log(JSON.stringify(mailchimp));

			var jqxhr = $.ajax({
				type: "POST",
				url: "/.netlify/functions/mailchimp/?list=rambridge",
				data: JSON.stringify(mailchimp),
			}).done(function(data) {
				// Trigger completion screen
				$(".newsletter-signup__thankyou").css("visibility", "visible");
				$("html, body").animate({ scrollTop: $(".newsletter-signup").offset().top }, 1000);

				// Reset the form just to be tidy
				$(form).trigger("reset");
			}).fail(function(err) {
				console.log(err.statusText);
				alert("Sorry, we were unable to process your subscription. Please try again later.")
			});

		}
	});

	$('form.newsletter-signup__form input').focusin(function () {
		console.log("focusin " + $(this).attr('name'));
		// console.log($("label[for=" + $(this).attr('name') + "]"));
		$("label[for=" + $(this).attr('name') + "]").find('span').css("visibility", "visible");
	});

	$('form.newsletter-signup__form input').focusout(function () {
		console.log("focusout " + $(this).attr('name'));
		// console.log($("label[for=" + $(this).attr('name') + "]"));
		if ($(this).valid()) {
			// console.log("field is valid");
			$("label[for=" + $(this).attr('name') + "]").find('span').css("visibility", "hidden");
		}
	});


	$('form.newsletter-signup__form input').keyup(function () {
		activateNewsletterButtons();
	});

	$('form.newsletter-signup__form input[type="checkbox"]').change(function () {
		console.log("Checkbox changed value");
		activateNewsletterButtons();
	});

	function activateNewsletterButtons() {
		if (rb.isStepValid( $('.newsletter-signup div.step.active') )) {
			console.log("Step is valid, let's move on");
			$('.newsletter-signup div.step.active ~ .form-controls button.next').prop("disabled", false);
			
			if ($('.newsletter-signup div.step.active').attr('data-step') === "2") {
				$('.newsletter-signup div.step.active ~ .form-controls button.submit').prop("disabled", false);
			}
		}	else {
			console.log("Step is not valid");
		}
	}
	

	$('.newsletter-signup button.next').click(function(event) {
		$('.newsletter-signup div.step.active').each(function() {
			$(this).removeClass('active').next('div.step').addClass('active');
		});
	});
	
	$('.newsletter-signup button.prev').click(function(event) {
		$('.newsletter-signup div.step.active').each(function() {
			$(this).removeClass('active').prev('div.step').addClass('active');
		});
	});

});

