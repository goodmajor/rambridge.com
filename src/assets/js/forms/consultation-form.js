import $ from 'jquery';
import validate from 'jquery-validation';
import 'jquery-validation/dist/additional-methods';
import rb from '../rb';

window.jQuery = $;

$(document).ready(function(){

	console.log("Consultation Form loaded...");
	
	// Disable the default submit method
	$('form.consultation__form')
	.submit(function(e){
		e.preventDefault();
  });

	var consultationValidator = $("form.consultation__form").validate({
		// debug: true,
		// ignore:'', // Required if you want to validate invisible elements (e.g. styled radios)
		groups: {
			'business-structure': 'is-sole-proprietorship is-partnership is-llc is-corporation',
			'business-type': 'is-retail-store is-acmpr is-micro-cultivation is-applicant',
			'monday-hours': 'monday-open-time-hour monday-open-time-minute monday-open-time-am-pm monday-close-time-hour monday-close-time-minute monday-close-time-am-pm',
			'tuesday-hours': 'tuesday-open-time-hour tuesday-open-time-minute tuesday-open-time-am-pm tuesday-close-time-hour tuesday-close-time-minute tuesday-close-time-am-pm',
			'wednesday-hours': 'wednesday-open-time-hour wednesday-open-time-minute wednesday-open-time-am-pm wednesday-close-time-hour wednesday-close-time-minute wednesday-close-time-am-pm',
			'thursday-hours': 'thursday-open-time-hour thursday-open-time-minute thursday-open-time-am-pm thursday-close-time-hour thursday-close-time-minute thursday-close-time-am-pm',
			'friday-hours': 'friday-open-time-hour friday-open-time-minute friday-open-time-am-pm friday-close-time-hour friday-close-time-minute friday-close-time-am-pm',
			'saturday-hours': 'saturday-open-time-hour saturday-open-time-minute saturday-open-time-am-pm saturday-close-time-hour saturday-close-time-minute saturday-close-time-am-pm',
			'sunday-hours': 'sunday-open-time-hour sunday-open-time-minute sunday-open-time-am-pm sunday-close-time-hour sunday-close-time-minute sunday-close-time-am-pm'
		},
		rules: {
			email: {
				email: true
			}
		},
		// Required as we've already placed labels ourselves
		errorClass: "error",
		errorElement: "span",
		errorPlacement: function(error, element) {
			console.log("ErrorPlacement");

			var inputType = element.attr('type');

			if (( inputType === 'checkbox' ) || ( inputType === 'radio')) {
				$( element )
					.closest( 'fieldset' )
						.find('input[type="' + inputType + '"]')
							.last()
								.closest('.cell')
									.after( $('<div class="cell error"></div>').append(error) );
	
			} else if ( inputType === 'file') {
				$( element )
					.siblings( "label[for='" + element.attr( "name" ) + "']" )
					.last()
						.after(error);
			
			} else if ( element.is('select')) {
				$( element )
					.closest( "label[for='" + element.attr( "name" ) + "']" )
						.after($('<div></div>').append(error));
			} else if ($(element).hasClass('time-hour') || $(element).hasClass('time-minute') || $(element).hasClass('time-am-pm')) {
				$(element).closest(".open-times").after(error);
			} else {
					error.insertAfter(element);
			}
		},

		invalidHandler: function(event, validator) {
			console.log("Form submit FAILURE");
			console.log(`There are ${validator.numberOfInvalids()} errors`);
			rb.dumpValues($(event.target));	
		},
		submitHandler: function(form) {
			$(form).find('button.submit').attr("disabled", true).text("Sending...");
			console.log("Submit");
			var rawFormData = new FormData(form);
			var newFormData = new FormData();

			newFormData.set('form-name', 'consultation-form-tidy')
			newFormData.set('Country', rawFormData.get('countrychooser'))
			
			newFormData.set('Business Name', rawFormData.get('businessname') ? rawFormData.get('businessname') :  "Not supplied")
			newFormData.set('Contact Name', rawFormData.get('contactname') ? rawFormData.get('contactname') :  "Not supplied")
			newFormData.set('Phone', rawFormData.get('phone') ? rawFormData.get('phone') :  "Not supplied")
			newFormData.set('Fax', rawFormData.get('fax') ? rawFormData.get('fax') :  "Not supplied")
			newFormData.set('Email', rawFormData.get('email') ? rawFormData.get('email') :  "Not supplied")
			newFormData.set('Website', rawFormData.get('website') ? rawFormData.get('website') :  "Not supplied")


			// Create formatted Business address string
			newFormData.set('Business Address', 
`${rawFormData.get('biz-streetaddress')}${ rawFormData.get('biz-unitnumber') ? ', ' + rawFormData.get('biz-unitnumber') : ''},
${rawFormData.get('biz-city')},
${rawFormData.get('biz-province')}${rawFormData.get('biz-state')} ${rawFormData.get('biz-postalcode')}${rawFormData.get('biz-zipcode')}`);

			// Create formatted Delivery address string
			if (rawFormData.get('add-delivery-address') === 'has-delivery-address') {
				newFormData.set('Delivery Address', 
				`${rawFormData.get('dlv-streetaddress')}${ rawFormData.get('dlv-unitnumber') ? ', ' + rawFormData.get('dlv-unitnumber') : ''},
${rawFormData.get('dlv-city')},
${rawFormData.get('dlv-province')}${rawFormData.get('dlv-state')} ${rawFormData.get('dlv-postalcode')}${rawFormData.get('dlv-zipcode')}`);
			} else {
				newFormData.set('Delivery Address', 'Same as business address');
			}

			newFormData.set('Invoice Method', rawFormData.get('invoice-method'))

			newFormData.set('Additional Buyer 1', rawFormData.get('additional-buyer-1') ? rawFormData.get('additional-buyer-1') :  "Not supplied")
			newFormData.set('Additional Buyer 2', rawFormData.get('additional-buyer-2') ? rawFormData.get('additional-buyer-2') :  "Not supplied")
			newFormData.set('Additional Buyer 3', rawFormData.get('additional-buyer-3') ? rawFormData.get('additional-buyer-3') :  "Not supplied")

			// Opening times
			const days = [
				'monday',
				'tuesday',
				'wednesday',
				'thursday',
				'friday',
				'saturday',
				'sunday'
			]

			days.forEach(function(day) {
				if (rawFormData.get('cons-open-' + day)) {
					newFormData.set(rb.capFirstLetter(day), 'Open ' +
						rawFormData.get(day + '-open-time-hour') + ':' + rawFormData.get(day + '-open-time-minute') + rawFormData.get(day + '-open-time-am-pm') + ' to ' +
						rawFormData.get(day + '-close-time-hour') + ':' + rawFormData.get(day + '-close-time-minute') + rawFormData.get(day + '-close-time-am-pm')
					)
				} else {
					newFormData.set(rb.capFirstLetter(day), 'Closed')
				}
			})

			newFormData.set('Has Forklift?', rawFormData.get('forklift') === "has-forklift" ? "Yes" : "No")
			newFormData.set('Need Liftgate?', rawFormData.get('liftgate') === "need-liftgate" ? "Yes" : "No")

			newFormData.set('Max Truck Size', rawFormData.get('max-truck-size')) 
			newFormData.set('Delivery Instructions', rawFormData.get('delivery-info') ? rawFormData.get('delivery-info') :  "Not supplied")

			newFormData.set('Retail Store', rawFormData.get('is-retail-store') ? "Yes" : "No")
			newFormData.set('ACMPR', rawFormData.get('is-acmpr') ? "Yes" : "No")
			newFormData.set('Micro Cultivation', rawFormData.get('is-micro-cultivation')  ? "Yes" : "No")
			newFormData.set('Current Applicant', rawFormData.get('is-applicant') ? "Yes" : "No")

			newFormData.set('Cultivation', rawFormData.get('cultivation') ? "Yes" : "No")
			newFormData.set('Sale', rawFormData.get('sale') ? "Yes" : "No")
			newFormData.set('Processing', rawFormData.get('processing') ? "Yes" : "No")
			newFormData.set('Cloning', rawFormData.get('cloning') ? "Yes" : "No")
			newFormData.set('Edibles', rawFormData.get('edibles') ? "Yes" : "No")

			var jqxhr = $.ajax(
				{
					url: $(form).attr("action"),
					timeout: 10000,
					type: 'POST',
					data: newFormData,
					processData: false,
					contentType: false
					// ^^^ Important when sending FormData object
			})		
			.done(function(data) {
				// Show completion message, hide footer and header
				$('form.consultation__form div.step-complete').addClass('active');
				$('form.consultation__form div.step.active').removeClass('active');
				// Hide form controls
				$('form.consultation__form .form-controls').css('display', 'none');
			})
			.fail(function(err) {
				console.log("Submission failure");
				$(form).find('button.submit').attr("disabled", false).text("Send");
				alert("Sorry, there was a problem processing your request.\n\nPlease try again, or call us toll-free at 1‑800‑265‑4769");
				console.log(err);
			});
		}	
	});

	$("form.consultation__form .con-business-type").each(function(){
		$(this).rules("add", {
			require_from_group: [1, ".con-business-type"]
		});
	});


	//==============
	// Form Actions
	//==============
	$('form.consultation__form button.next').click(function(event) {
		if (rb.isStepValid( $('form.consultation__form div.step.active') )) {
			console.log("Step is valid, let's move on");
			// This will take care of header, progress and form step
			$('form.consultation__form div.step.active').each(function() {
				$(this).removeClass('active').next('div.step').addClass('active');
			});

			// Scroll to top
			$('section.consultation').get(0).scrollIntoView({behavior: "smooth"});
					
			// Focus first field
			$('form.consultation__form div.step.active').find("input, select").first().focus();
		}	else {
			console.log("Step is not valid");
			// Focus first invalid field
			$('form.consultation__form div.step.active').find("input.error, select.error").first().focus();
		}
	});
	
	$('form.consultation__form button.prev').click(function(event) {
		// console.log("prev")
		$('form.consultation__form div.step.active').each(function() {
			console.log(this);
			$(this).removeClass('active').prev('div.step').addClass('active');
		});

		// Scroll to top
		$('section.consultation').get(0).scrollIntoView({behavior: "smooth"});

	});

	// Prepend an asterisk to placeholder text for required fields
	$('form.consultation__form')
	.find("input[type=textarea]:required, input[type=text]:required").each(function(ev)
	{
		if(!$(this).val()) { 
			let ph = $(this).attr("placeholder");
				$(this).attr("placeholder", "* " + ph);
		}
	});



	// By default, show Canadian account fields
	$("form.consultation__form div.cell.usa-only").css("display", "none");
	$("form.consultation__form div.cell.canada-only").css("display", "block");

	// Update country-specific account fields when dropdown changes
	$( "form.consultation__form #countrychooser").change(function () {    
		console.log(`Switched country to ${$(this).val()}`);
		var thisform = $(this).closest('form');
		if ($(this).val() === "canada" ) {
			$(thisform).find("div.cell.usa-only").css("display", "none");
			$(thisform).find("div.cell.canada-only").css("display", "block");
			$(thisform).find("div.cell.usa-only input").val("");
		} 
		if ($(this).val() === "usa" ) {
			$(thisform).find("div.cell.canada-only").css("display", "none");
			$(thisform).find("div.cell.usa-only").css("display", "block");
			$(thisform).find("div.cell.canada-only input").val("");
		}  
		// Reset any form errors when switching countries
		consultationValidator.resetForm();
		$("form.consultation__form .labeltext").css("visibility", "hidden");
	});  

	// By default, hide retail licence number field
	$("form.consultation__form .retail-store-only").css("display", "none");

	// Request licence number if retail store
	// $( "form.consultation__form #con-is-retail-store" ).change(function () {    
	// 	console.log('Retail store clicked');
	// 	if ($(this).is(":checked") ) {
	// 		$(this).closest("form").find('.retail-store-only').css("display", "block");
	// 	} else {
	// 		$(this).closest("form").find('.retail-store-only').css("display", "none");
	// 	} 
	// });  

	// By default, don't show the Delivery Address section
	$("form.consultation__form .delivery-address").css("display", "none");

	// Expand Delivery Address section if required
	$( "form.consultation__form input[name='add-delivery-address']" ).change(function () { 
		console.log('Delivery address clicked');
		if ($(this).val() === "has-delivery-address") {
			$(this).closest('form').find(".delivery-address").css("display", "flex");
		} else {
			$(this).closest('form').find(".delivery-address").css("display", "none");
		}
		// Clear any previously filled-in fields
		$(this).find(".delivery-address input").val("");
	});  

	// Set sensible opening time defaults when selecting "we're open today"
	$("form.consultation__form .open-times .day input.open-days").change(function() {
		console.log("Consultation form hours");
		$(this).closest(".open-times").children(".open-time").find("input.time-hour").val("10").valid();
		$(this).closest(".open-times").children(".open-time").find("input.time-minute").val("00").valid();
		$(this).closest(".open-times").children(".close-time").find("input.time-hour").val("4").valid();
		$(this).closest(".open-times").children(".close-time").find("input.time-minute").val("00").valid();
		$(this).closest(".open-times").children(".open-time, .separator, .close-time, .closed-today").toggle();
	});

	// Force file input types to re-validate whenever they change
	// so we can clear any validation messages immediately
	// IDK why this isn't default behaviour
	$('form.consultation__form input[type="file"]').on('change', function(e) {
		var input	= $( this );
		var label	= input.next( 'label' );

		// Remove existing filename if it exists
		label.next('.filename').empty();
		
		if ( input.valid() ) {

			// Add filename and "Remove" button, after input label
			var filename = e.target.value.split( '\\' ).pop();
			var removeLink = $('<div class="remove-file" data-remove-file=' + e.target.id + '>REMOVE</div>');
			
			label.after( 
				$('<div class="filename"></div>')
				.append("<div><strong>Filename:</strong> " + filename + "</div>")
				.append(removeLink)
			);
			label.addClass('filled');

			// Attach event listener to the newly-created REMOVE button
			removeLink.on('click', function(e) {
				console.log("Remove file");
				var targetId = ($(e.target).attr("data-remove-file"));
				// Clear file input value, and trigger re-validation
				$('#' + targetId)
					.val('')
					.trigger( "change" );
			})
		} else {
			label.removeClass('filled');
			label.siblings('span.filename').remove();
		}    
	});



	function processApplicationFormValues(form) {
		var allInputs = $(form).find('input, select');	
		var formValues = {};
		allInputs.each(function() {
			if ($(this).attr('type') === "checkbox") {
				formValues[this.name] = $(this).prop('checked');
			} else if ($(this).attr('type') === "radio") {
				if ( $(this).prop('checked') ) {
					formValues[this.name] = this.id;
				}
			} else {
				formValues[this.name] = $(this).val();				
			}
		});
		return(formValues);
	}
});