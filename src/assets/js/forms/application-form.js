import $ from 'jquery';
import validate from 'jquery-validation';
import 'jquery-validation/dist/additional-methods';
import rb from '../rb';

window.jQuery = $;

$(document).ready(function(){

	console.log("Application Form loaded...");
	//dumpValues($("form#applicationForm"));	
	
	// Disable the default submit method
	$('form.create-account__application-form')
	.submit(function(e){
		e.preventDefault();
  });

	var applicationValidator = $("form.create-account__application-form").validate({
		// debug: true,
		// ignore:'', // Required if you want to validate invisible elements (e.g. styled radios)
		groups: {
			'business-structure': 'is-sole-proprietorship is-partnership is-llc is-corporation',
			'business-type': 'is-retail-store is-plants-nursery is-garden-supplies is-hardware-plumbing is-feed is-propagation-nursery',
			'monday-hours': 'monday-open-time-hour monday-open-time-minute monday-open-time-am-pm monday-close-time-hour monday-close-time-minute monday-close-time-am-pm',
			'tuesday-hours': 'tuesday-open-time-hour tuesday-open-time-minute tuesday-open-time-am-pm tuesday-close-time-hour tuesday-close-time-minute tuesday-close-time-am-pm',
			'wednesday-hours': 'wednesday-open-time-hour wednesday-open-time-minute wednesday-open-time-am-pm wednesday-close-time-hour wednesday-close-time-minute wednesday-close-time-am-pm',
			'thursday-hours': 'thursday-open-time-hour thursday-open-time-minute thursday-open-time-am-pm thursday-close-time-hour thursday-close-time-minute thursday-close-time-am-pm',
			'friday-hours': 'friday-open-time-hour friday-open-time-minute friday-open-time-am-pm friday-close-time-hour friday-close-time-minute friday-close-time-am-pm',
			'saturday-hours': 'saturday-open-time-hour saturday-open-time-minute saturday-open-time-am-pm saturday-close-time-hour saturday-close-time-minute saturday-close-time-am-pm',
			'sunday-hours': 'sunday-open-time-hour sunday-open-time-minute sunday-open-time-am-pm sunday-close-time-hour sunday-close-time-minute sunday-close-time-am-pm'
		},
		rules: {
			email: {
				email: true
			}
		},
		// Required as we've already placed labels ourselves
		errorClass: "error",
		errorElement: "span",
		errorPlacement: function(error, element) {
			console.log("ErrorPlacement");

			var inputType = element.attr('type');

			if (( inputType === 'checkbox' ) || ( inputType === 'radio')) {
				$( element )
					.closest( 'fieldset' )
						.find('input[type="' + inputType + '"]')
							.last()
								.closest('.cell')
									.after( $('<div class="cell error"></div>').append(error) );
	
			} else if ( inputType === 'file') {
				$( element )
					.siblings( "label[for='" + element.attr( "name" ) + "']" )
					.last()
						.after(error);
			
			} else if ( element.is('select')) {
				$( element )
					.closest( "label[for='" + element.attr( "name" ) + "']" )
						.after($('<div></div>').append(error));
			} else if ($(element).hasClass('time-hour') || $(element).hasClass('time-minute') || $(element).hasClass('time-am-pm')) {
				$(element).closest(".open-times").after(error);
			} else {
					error.insertAfter(element);
			}
		},

		invalidHandler: function(event, validator) {
			console.log("Form submit FAILURE");
			console.log(`There are ${validator.numberOfInvalids()} errors`);
			rb.dumpValues($(event.target));	
		},
		submitHandler: function(form) {
			$(form).find('button.submit').attr("disabled", true).text("Sending...");
			console.log("Submit");
			var rawFormData = new FormData(form);
			var newFormData = new FormData();

			newFormData.set('form-name', 'application-form-tidy')
			newFormData.set('Country', rawFormData.get('countrychooser'))

			newFormData.set('Business Name', rawFormData.get('businessname') ? rawFormData.get('businessname') :  "Not supplied")
			newFormData.set('Contact Name', rawFormData.get('contactname') ? rawFormData.get('contactname') :  "Not supplied")
			newFormData.set('Phone', rawFormData.get('phone') ? rawFormData.get('phone') :  "Not supplied")
			newFormData.set('Fax', rawFormData.get('fax') ? rawFormData.get('fax') :  "Not supplied")
			newFormData.set('Email', rawFormData.get('email') ? rawFormData.get('email') :  "Not supplied")
			newFormData.set('Website', rawFormData.get('website') ? rawFormData.get('website') :  "Not supplied")

			// Create formatted Business address string
			newFormData.set('Business Address', 
`${rawFormData.get('biz-streetaddress')}${ rawFormData.get('biz-unitnumber') ? ', ' + rawFormData.get('biz-unitnumber') : ''},
${rawFormData.get('biz-city')},
${rawFormData.get('biz-province')}${rawFormData.get('biz-state')} ${rawFormData.get('biz-postalcode')}${rawFormData.get('biz-zipcode')}`);

			// Create formatted Delivery address string
			if (rawFormData.get('add-delivery-address') === "has-delivery-address") {
				newFormData.set('Delivery Address', 
`${rawFormData.get('dlv-streetaddress')}${ rawFormData.get('dlv-unitnumber') ? ', ' + rawFormData.get('dlv-unitnumber') : ''}, 
${rawFormData.get('dlv-city')},
${rawFormData.get('dlv-province')}${rawFormData.get('dlv-state')} ${rawFormData.get('dlv-postalcode')}${rawFormData.get('dlv-zipcode')}`);
			} else {
				newFormData.set('Delivery Address', "Same as business address");
			}

			newFormData.set('Federal Tax ID', rawFormData.get('tax-id') ? rawFormData.get('tax-id') :  "Not supplied")
			newFormData.set('Years in Business', rawFormData.get('years-in-business') ? rawFormData.get('years-in-business') :  "Not supplied")
			newFormData.set('Invoice Method', rawFormData.get('invoice-method'))

			newFormData.set('Retail Store', rawFormData.get('is-retail-store') ? "Yes" : "No")
			newFormData.set('Retail Licence Number', rawFormData.get('retail-licence-number') ? rawFormData.get('retail-licence-number') :  "Not supplied")
			newFormData.set('Plants / Nursery', rawFormData.get('is-plants-nursery')  ? "Yes" : "No")
			newFormData.set('Garden Supply', rawFormData.get('is-garden-supplies')  ? "Yes" : "No")
			newFormData.set('Hardware Plumbing', rawFormData.get('is-hardware-plumbing') ? "Yes" : "No")
			newFormData.set('Feed', rawFormData.get('is-feed') ? "Yes" : "No")
			newFormData.set('Propagation Nursery', rawFormData.get('is-propagation-nursery') ? "Yes" : "No")

			newFormData.set('Additional Info', rawFormData.get('additional-info') ? rawFormData.get('additional-info') :  "Not supplied")

			newFormData.set('Additional Buyer 1', rawFormData.get('additional-buyer-1') ? rawFormData.get('additional-buyer-1') :  "Not supplied")
			newFormData.set('Additional Buyer 2', rawFormData.get('additional-buyer-2') ? rawFormData.get('additional-buyer-2') :  "Not supplied")
			newFormData.set('Additional Buyer 3', rawFormData.get('additional-buyer-3') ? rawFormData.get('additional-buyer-3') :  "Not supplied")

			newFormData.set('Type of Business', rawFormData.get('app-business-structure'))

			newFormData.set('Store with Public Access', rawFormData.get('store-with-public-access') === "has-public-access" ? "Yes" : "No")

			// Opening times
			const days = [
				'monday',
				'tuesday',
				'wednesday',
				'thursday',
				'friday',
				'saturday',
				'sunday'
			]

			days.forEach(function(day) {
				if (rawFormData.get('open-' + day)) {
					newFormData.set(rb.capFirstLetter(day), 'Open ' +
						rawFormData.get(day + '-open-time-hour') + ':' + rawFormData.get(day + '-open-time-minute') + rawFormData.get(day + '-open-time-am-pm') + ' to ' +
						rawFormData.get(day + '-close-time-hour') + ':' + rawFormData.get(day + '-close-time-minute') + rawFormData.get(day + '-close-time-am-pm')
					)
				} else {
					newFormData.set(rb.capFirstLetter(day), 'Closed')
				}
			})

			newFormData.set('Has Forklift?', rawFormData.get('forklift') === "has-forklift" ? "Yes" : "No")
			newFormData.set('Need Liftgate?', rawFormData.get('liftgate') === "need-liftgate" ? "Yes" : "No")

			newFormData.set('Max Truck Size', rawFormData.get('max-truck-size')) 
			newFormData.set('Delivery Instructions', rawFormData.get('delivery-info')) 

			let emptyFile = new File(["Not provided"], 'not-provided.txt', {
				type: "text/plain",
				lastModified: Date.now(),
			})

			newFormData.set('Interior Photo',   rawFormData.get('interior-photo-upload').size   ? rawFormData.get('interior-photo-upload')   : emptyFile) 
			newFormData.set('Exterior Photo',   rawFormData.get('exterior-photo-upload').size   ? rawFormData.get('exterior-photo-upload')   : emptyFile)
			newFormData.set('Sellers Permit',   rawFormData.get('sellers-permit-upload').size   ? rawFormData.get('sellers-permit-upload')   : emptyFile)
			newFormData.set('Business License', rawFormData.get('business-license-upload').size ? rawFormData.get('business-license-upload') : emptyFile)
			newFormData.set('Nursery License',  rawFormData.get('nursery-license-upload').size  ? rawFormData.get('nursery-license-upload')  : emptyFile)

			newFormData.set('Newsletter Opt-In', rawFormData.get('store-with-public-access') === "has-public-access" ? "Yes" : "No")
			
			var jqxhr = $.ajax(
				{
					url: $(form).attr("action"),
					timeout: 10000,
					type: 'POST',
					data: newFormData,
					processData: false,
					contentType: false
					// ^^^ Important when sending FormData object
			})		
			.done(function(data) {
				// Show completion message, hide footer and header
				$('#main-menu__create-account div.step.active').each(function() {
					console.log(this);
					$(this).removeClass('active').next('div.step').addClass('active');
				});
				// Hide form controls
				$('form.create-account__application-form .form-controls').css('display', 'none');
			})
			.fail(function(err) {
				console.log("Submission failure");
				$(form).find('button.submit').attr("disabled", false).text("Send");
				alert("Sorry, there was a problem processing your application.\n\nPlease try again, or call us toll-free at 1‑800‑265‑4769");
				console.log(err);
			});
		}	
	});

	$("form.create-account__application-form .app-business-type").each(function(){
		$(this).rules("add", {
			require_from_group: [1, ".app-business-type"]
		});
	});

	// Override built-in default validation error messages 
	$.extend($.validator.messages, { 
		// required: "Required field",
		required: function (params, el) {
			if ($(el).attr('type') === "radio") {
				return "Please select an option";
			}
			return "Required field";
		},
		"require_from_group": "Please select at least one option",
		// remote: "Please fix this field.",
    email: "Please enter a valid email address.",
    // url: "Please enter a valid URL.",
    // date: "Please enter a valid date.",
    // dateISO: "Please enter a valid date (ISO).",
    // number: "Please enter a valid number.",
    // digits: "Please enter only digits.",
    // creditcard: "Please enter a valid credit card number.",
    // equalTo: "Please enter the same value again.",
    accept: "Please select a PDF, PNG, JPG or MS Word file",
    // maxlength: jQuery.validator.format("Please enter no more than {0} characters."),
    // minlength: jQuery.validator.format("Please enter at least {0} characters."),
    // rangelength: jQuery.validator.format("Please enter a value between {0} and {1} characters long."),
    // range: jQuery.validator.format("Please enter a value between {0} and {1}."),
    // max: jQuery.validator.format("Please enter a value less than or equal to {0}."),
    // min: jQuery.validator.format("Please enter a value greater than or equal to {0}.")
	});
	
	jQuery.validator.addMethod("filesize_max", function(value, element, param) {
    var isOptional = this.optional(element), file;
    if(isOptional) {
       return isOptional;
    }
    1
    if ($(element).attr("type") === "file") {
      if (element.files && element.files.length) {
        file = element.files[0];            
        	return ( file.size && file.size <= (param*1024*1024) ); 
        }
    	}
    	return false;
	}, function(param, el) {
		return `Please select a file less than ${param} megabyte(s) in size`;
	});

	$.validator.addClassRules("document-upload", {
		accept: "application/pdf, image/jpeg, image/png, application/msword, application/vnd.openxmlformats-officedocument.wordprocessingml.document",
		filesize_max: 2
	});
	
	$.validator.addMethod("validTime", $.validator.methods.range,
				"Please input valid opening and closing times");

	$.validator.addClassRules("time-hour", {
		required: true,
		validTime: [1, 12]
	});

	$.validator.addClassRules("time-minute", {
		required: true,
		validTime: [0, 59]
	});

	//==============
	// Form Actions
	//==============
	$('form.create-account__application-form button.next').click(function(event) {
		if (rb.isStepValid( $('form.create-account__application-form div.step.active') )) {
			console.log("Step is valid, let's move on");
			// This will take care of header, progress and form step
			$('#main-menu__create-account div.step.active').each(function() {
				$(this).removeClass('active').next('div.step').addClass('active');
			});
			// Scroll to top
			$('.menu-scroll-container ').animate({ scrollTop: 0 });
					
			// Focus first field
			$('form.create-account__application-form div.step.active').find("input, select").first().focus();
		}	else {
			console.log("Step is not valid");
			// Focus first invalid field
			$('form.create-account__application-form div.step.active').find("input.error, select.error").first().focus();
		}
	});
	
	$('form.create-account__application-form button.prev').click(function(event) {
		// console.log("prev")
		$('#main-menu__create-account div.step.active').each(function() {
			console.log(this);
			$(this).removeClass('active').prev('div.step').addClass('active');
		});
		// Scroll to top
		$('.menu-scroll-container').animate({ scrollTop: 0 });

	});

	// Prepend an asterisk to placeholder text for required fields
	$('form.create-account__application-form')
	.find("input[type=textarea]:required, input[type=text]:required").each(function(ev)
	{
		if(!$(this).val()) { 
			let ph = $(this).attr("placeholder");
				$(this).attr("placeholder", "* " + ph);
		}
	});


	// By default, show Canadian account fields
	$("form.create-account__application-form div.cell.usa-only").css("display", "none");
	$("form.create-account__application-form div.cell.canada-only").css("display", "block");

	// Update country-specific account fields when dropdown changes
	$( "form.create-account__application-form #countrychooser").change(function () {    
		console.log(`Switched country to ${$(this).val()}`);
		var thisform = $(this).closest('form');
		if ($(this).val() === "Canada" ) {
			$(thisform).find("div.cell.usa-only").css("display", "none");
			$(thisform).find("div.cell.canada-only").css("display", "block");
			$(thisform).find("div.cell.usa-only input").val("");
		} 
		if ($(this).val() === "USA" ) {
			$(thisform).find("div.cell.canada-only").css("display", "none");
			$(thisform).find("div.cell.usa-only").css("display", "block");
			$(thisform).find("div.cell.canada-only input").val("");
		}  
		// Reset any form errors when switching countries
		applicationValidator.resetForm();
		$("form.create-account__application-form .labeltext").css("visibility", "hidden");
	});  

	// By default, hide retail licence number field
	$("form.create-account__application-form .retail-store-only").css("display", "none");

	// Request licence number if retail store
	$( "form.create-account__application-form #app-is-retail-store" ).change(function () {    
		console.log('Retail store clicked');
		if ($(this).is(":checked") ) {
			$(this).closest("form").find('.retail-store-only').css("display", "block");
		} else {
			$(this).closest("form").find('.retail-store-only').css("display", "none");
		} 
	});  

	// By default, don't show the Delivery Address section
	$("form.create-account__application-form .delivery-address").css("display", "none");

	// Expand Delivery Address section if required
	$( "form.create-account__application-form input[name='add-delivery-address']" ).change(function () { 
		console.log('Delivery address clicked');
		if ($(this).val() === "has-delivery-address") {
			$(this).closest('form').find(".delivery-address").css("display", "flex");
		} else {
			$(this).closest('form').find(".delivery-address").css("display", "none");
		}
		// Clear any previously filled-in fields
		$(this).find(".delivery-address input").val("");
	});  

	// Set sensible opening time defaults when selecting "we're open today"
	$("form.create-account__application-form .open-times .day input.open-days").change(function() {
		console.log("Application form hours");
		$(this).closest(".open-times").children(".open-time").find("input.time-hour").val("10").valid();
		$(this).closest(".open-times").children(".open-time").find("input.time-minute").val("00").valid();
		$(this).closest(".open-times").children(".close-time").find("input.time-hour").val("4").valid();
		$(this).closest(".open-times").children(".close-time").find("input.time-minute").val("00").valid();
		$(this).closest(".open-times").children(".open-time, .separator, .close-time, .closed-today").toggle();
	});

	// Force file input types to re-validate whenever they change
	// so we can clear any validation messages immediately
	// IDK why this isn't default behaviour
	$('form.create-account__application-form input[type="file"]').on('change', function(e) {
		var input	= $( this );
		var label	= input.next( 'label' );

		// Remove existing filename if it exists
		label.next('.filename').empty();
		
		if ( input.valid() ) {

			// Add filename and "Remove" button, after input label
			var filename = e.target.value.split( '\\' ).pop();
			var removeLink = $('<div class="remove-file" data-remove-file=' + e.target.id + '>REMOVE</div>');
			
			label.after( 
				$('<div class="filename"></div>')
				.append("<div><strong>Filename:</strong> " + filename + "</div>")
				.append(removeLink)
			);
			label.addClass('filled');

			// Attach event listener to the newly-created REMOVE button
			removeLink.on('click', function(e) {
				console.log("Remove file");
				var targetId = ($(e.target).attr("data-remove-file"));
				// Clear file input value, and trigger re-validation
				$('#' + targetId)
					.val('')
					.trigger( "change" );
			})
		} else {
			label.removeClass('filled');
			label.siblings('span.filename').remove();
		}    
	});



	function processApplicationFormValues(form) {
		var allInputs = $(form).find('input, select');	
		var formValues = {};
		allInputs.each(function() {
			if ($(this).attr('type') === "checkbox") {
				formValues[this.name] = $(this).prop('checked');
			} else if ($(this).attr('type') === "radio") {
				if ( $(this).prop('checked') ) {
					formValues[this.name] = this.id;
				}
			} else {
				formValues[this.name] = $(this).val();				
			}
		});
		return(formValues);
	}
});