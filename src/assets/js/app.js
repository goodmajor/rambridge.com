import $ from 'jquery';
import 'what-input';
import AOS from 'aos';
import slick from 'slick-carousel';
import './locator';
import './forms/application-form';
import './forms/consultation-form';
import './forms/newsletter-form';
import rb from './rb';
import 'formdata-polyfill';

import smoothscroll from 'smoothscroll-polyfill';

// kick off the polyfill!
smoothscroll.polyfill();

// Foundation JS relies on a global varaible. In ES6, all imports are hoisted
// to the top of the file so if we used`import` to import Foundation,
// it would execute earlier than we have assigned the global variable.
// This is why we have to use CommonJS require() here since it doesn't
// have the hoisting behavior.
window.jQuery = $;
require('foundation-sites');

// If you want to pick and choose which modules to include, comment out the above and uncomment
// the line below
//import './lib/foundation-explicit-pieces';


$(document).foundation();


var scrollStop = function (callback) {
	// Make sure a valid callback was provided
	if (!callback || typeof callback !== 'function') return;
	// Setup scrolling variable
	var isScrolling;
	// Listen for scroll events
	window.addEventListener('scroll', function (event) {
		// Clear our timeout throughout the scroll
		window.clearTimeout(isScrolling);
		// Set a timeout to run after scrolling ends
		isScrolling = setTimeout(function () {
			// Run the callback
			callback();
		}, 100);
	}, false);
};

$(document).ready(function () {

	// Set up the Feature Products carousel
	$("section.feature-products .carousel").slick({
		dots: true,
		arrows: false,
		touchThreshold: 8,
		appendDots: 'section.feature-products .slick-dots-container',
		adaptiveHeight: false,
		autoplay: true,
		autoplaySpeed: 5000
	});

	// Show link for first product
	$("section.feature-products .feature-products__product-link").first().addClass('active');

	// Change the associated Featured product text on slide change
	$("section.feature-products .feature-products__carousel").on('beforeChange', function (event, slick, previousSlide, currentSlide) {
		$('section.feature-products .feature-products__product-link').each(function (index) {
			if (index === currentSlide) {
				$(this).addClass('active');
			} else {
				$(this).removeClass('active');
			}
		});
	});

	// Test carousel
	// $('.test-carousel').slick({
	// 	infinite: true,
	// 	slidesToShow: 2,
	// 	slidesToScroll: 1,
	// 	autoplay: true,
	// 	autoplaySpeed: 2000,
		// variableWidth: true,
		// centerMode: true,
	// 	touchThreshold: 8,
	// });

	// Set up the Latest Products carousel
	$("section.latest-products .latest-products__carousel").slick({
		dots: true,
		arrows: false,
		touchThreshold: 8,
		appendDots: 'section.latest-products .slick-dots-container',
		adaptiveHeight: false,
		autoplay: true,
		autoplaySpeed: 5000
	});

	// Show links etc. for first product
	$('section.latest-products .latest-products__product-name').first().addClass('active');
	$('section.latest-products button.latest-products__quick-product-link').first().addClass('active');
	$('section.latest-products a.latest-products__cta').first().addClass('active');

	// Change the associated Popular product text on slide change
	$("section.latest-products .latest-products__carousel").on('beforeChange', function (event, slick, previousSlide, currentSlide) {
		// Update product name
		$('section.latest-products .latest-products__product-name').each(function (index) {
			if (index === currentSlide) {
				$(this).addClass('active');
			} else {
				$(this).removeClass('active');
			}
		});
		// Update quick product info link
		$('section.latest-products button.latest-products__quick-product-link').each(function (index) {
			if (index === currentSlide) {
				$(this).addClass('active');
			} else {
				$(this).removeClass('active');
			}
		});
		// Update quick product info link
		$('section.latest-products a.latest-products__cta').each(function (index) {
			if (index === currentSlide) {
				$(this).addClass('active');
			} else {
				$(this).removeClass('active');
			}
		});
	});

	// Set up the Product Details Page carousel
	$(".product-details .product-details__carousel").slick({
		dots: true,
		arrows: false,
		touchThreshold: 8,
		appendDots: 'section.product-details__images .product-details__carousel-nav .slick-dots-container',
		adaptiveHeight: false,
		autoplay: true,
		autoplaySpeed: 5000
	});

	// Set up the CEA carousel
	$(".cea-carousel .cea-carousel__carousel").slick({
		dots: true,
		arrows: true,
		touchThreshold: 8,
		appendDots: 'section.cea-carousel .carousel-nav .slick-dots-container',
		adaptiveHeight: false,
		autoplay: true,
		autoplaySpeed: 2500,
		slidesToShow: 3,
		slidesToScroll: 1,
		centerMode: false,
		responsive: [
			{
				breakpoint: 768,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1,
					centerMode: true,
					centerPadding: '10%',
				}
			}
		]
	});


	// Set up the Nursey Products carousels
	// #1
	$("section#nursery-products-1 .carousel").slick({
		dots: true,
		arrows: false,
		touchThreshold: 8,
		appendDots: 'section#nursery-products-1 .slick-dots-container',
		adaptiveHeight: false,
		autoplay: true,
		autoplaySpeed: 5000
	});

	// Show link for first product
	$("section#nursery-products-1 .nursery-products__product-link").first().addClass('active');

	// Change the associated  product text on slide change
	$("section#nursery-products-1 .nursery-products__carousel").on('beforeChange', function (event, slick, previousSlide, currentSlide) {
		$('section#nursery-products-1 .nursery-products__product-link').each(function (index) {
			if (index === currentSlide) {
				$(this).addClass('active');
			} else {
				$(this).removeClass('active');
			}
		});
	});
	// End #1

	// Set up the Nursey Products carousels
	// #2
	$("section#nursery-products-2 .carousel").slick({
		dots: true,
		arrows: false,
		touchThreshold: 8,
		appendDots: 'section#nursery-products-2 .slick-dots-container',
		adaptiveHeight: false,
		autoplay: true,
		autoplaySpeed: 5000
	});

	// Show link for first product
	$("section#nursery-products-2 .nursery-products__product-link").first().addClass('active');

	// Change the associated  product text on slide change
	$("section#nursery-products-2 .nursery-products__carousel").on('beforeChange', function (event, slick, previousSlide, currentSlide) {
		$('section#nursery-products-2 .nursery-products__product-link').each(function (index) {
			if (index === currentSlide) {
				$(this).addClass('active');
			} else {
				$(this).removeClass('active');
			}
		});
	});
	// End #2


	// Apply class to logo when the page scrolls, remove it after we stop scrolling
	// Not to be confused with use of Animate-on-scroll (AOS) library below
	// var scrollAnimationItems = $('.hide-on-scroll');

	// window.addEventListener('scroll', function() {
	// 	for (let element of scrollAnimationItems) {
	// 		element.classList.add('is-scrolling');
	// 	}
	// });

	// scrollStop(function () {
	// 	for (let element of scrollAnimationItems) {
	// 		element.classList.remove('is-scrolling');
	// 	}
	// });
	// End 

	// Initialize Animate-on-scroll library
	AOS.init({
		once: true
	});

	// $('#login-button').on("click", function() {
	// 	alert("Go to Login page on Business Central portal");
	// });

	$('.nav__button--home, .nav__link--home').on("click", function () {
		if (window.location.pathname === "/rambridge.html") {
			// $('.reveal').foundation('close');	
			$('.off-canvas').foundation('close');
			$('html, body').animate({
				scrollTop: ($('body').offset().top)
			}, 500);

		} else {
			window.location.href = "/rambridge.html";
		}
	});

	$('.nav__link--cea-home').on("click", function () {
		if (window.location.pathname === "/cea" || window.location.pathname === "/cea/" || window.location.pathname === "/cea/index.html") {
			// $('.reveal').foundation('close');	
			$('.off-canvas').foundation('close');
			$('html, body').animate({
				scrollTop: ($('body').offset().top)
			}, 500);

		} else {
			window.location.href = "/cea/index.html";
		}
	});

	// Hide the icon for the Locator tool when we scroll down the page
	// There are other ways to access it further down the page
	let modalIsOpen;

	$('[data-open="information-modal"]').on('click', function () {
		modalIsOpen = true;
		console.log("A modal was opened");
	});
	$('#information-modal [data-close]').on('click', function () {
		modalIsOpen = false;
		console.log("A modal was closed");
	});
	$('.reveal').on('open.zf.reveal', function () {
		modalIsOpen = true;
		console.log("A modal was opened");
	});
	$('.reveal').on('closed.zf.reveal', function () {
		modalIsOpen = false;
		console.log("A modal was closed");
	});

	// TODO - rate limit this
	$(window).scroll(function () {
		// console.log($(this).scrollTop());
		if (modalIsOpen == true) { return };
		if ($(this).scrollTop() > 200) {
			$('.nav__button--hide-on-scroll').addClass('nav__button--hidden');
		} else {
			$('.nav__button--hide-on-scroll').removeClass('nav__button--hidden');
		}
	});

	// Prevent main body scrolling when menu is open
	// Only really required on desktop
	$(document).on('opened.zf.offCanvas', function () {
		console.log("Menu opened");
		$("body").addClass("noscroll")
	});

	$(document).on('close.zf.offCanvas', function () {
		console.log("Menu closed");
		rb.resetMenu()
		$("body").removeClass("noscroll")
	});

	$('.nav__trigger--main').on('click', function () {
		console.log("Open MAIN MENU");
		rb.resetMenu()
		$('#rb-off-canvas').foundation('open');
		$('#main-menu-tabs').foundation('selectTab', 'main-menu__default');
	});

	$('.nav__trigger--cea').on('click', function () {
		console.log("Open CEA menu content");

		if ($("div.nav__main-menu-container ul.accordion li.is-active #cea").length) {
			console.log("CEA is already active")
			$('#rb-off-canvas').foundation('open');
		} else {
			console.log("CEA is not active")
			rb.resetMenu()
			$('#main-menu-tabs').foundation('selectTab', 'main-menu__default');
			if ($(".nav__menu-top").css('display') === 'none') {
				var menuTop = 0;
			} else {
				var menuTop = $(".nav__menu-top").outerHeight();
			}
			// console.log(menuTop)
			var position = ($(".menu-scroll-container ul.accordion").position())
			// console.log("position:", position)
			var offset = ($(".menu-scroll-container ul.accordion").offset())
			// console.log("offset:", offset)

			$('#rb-off-canvas').foundation('open');

			if (offset.top > 100) {
				console.log("Need to scroll")
				$(".menu-scroll-container").animate({ scrollTop: position.top - menuTop }, 700);
			} else {
				console.log("No need to scroll, already near the top")
			}
			$('.nav__main-menu-container ul.accordion').foundation('down', $('#cea'));
		}

	});

	$('.nav__trigger--toggle').on('click', function () {
		console.log("Toggle menu");
		rb.resetMenu()
		$('#rb-off-canvas').foundation('toggle');
	});

	$('.nav__trigger--back').on('click', function () {
		console.log("Go back");
		history.back();
	});

	$('.nav__trigger--close').on('click', function () {
		console.log("Close menu");
		rb.resetMenu()
		$('#rb-off-canvas').foundation('close');
	});

	$('.nav__trigger--wholesale').on('click', function () {
		console.log("Open WHOLESALE");
		rb.resetMenu();
		$('#rb-off-canvas').foundation('open');
		$('#main-menu-tabs').foundation('selectTab', 'main-menu__wholesale');
	});

	$('.nav__trigger--create-account').on('click', function () {
		console.log("Open CREATE ACCOUNT");
		rb.resetMenu()
		$('#rb-off-canvas').foundation('open');
		$('#main-menu-tabs').foundation('selectTab', 'main-menu__create-account');
	});

	$('.nav__trigger--contact').on('click', function () {
		console.log("Open CONTACT FORM");
		rb.resetMenu()
		$('#rb-off-canvas').foundation('open');
		$('#main-menu-tabs').foundation('selectTab', 'main-menu__contact');
	});

	$('.nav__trigger--consultation-form').on('click', function () {
		console.log("Go to CONSULTATION FORM");
		rb.resetMenu()
		$("body,html").animate(
			{
				scrollTop: $('#consultation-form').offset().top
			}, 700 //speed
		);
	});

	$('.nav__trigger--faq').on('click', function () {
		console.log("Go to FAQS");
		rb.resetMenu()
		$('#main-menu-tabs').foundation('selectTab', 'main-menu__default');
		$('.nav__main-menu-container > .accordion').foundation('down', $('.nav__main-menu-container > .accordion #faqs'));
		$(".menu-scroll-container").animate(
			{
				scrollTop: $('#faqs').offset().top - $('.menu-scroll-container .nav__menu-top').height()
			}, 700 //speed
		);
	});

	$('.tabs').on('change.zf.tabs', function (event) {
		var activeTab = $(this).find('.is-active a').attr('href');
		// console.log(activeTab);
		if ((activeTab === "#support-videos") || (activeTab === "#support-pdfs")) {
			Foundation.Motion.animateIn($('' + activeTab), "fade-in");
		} else {
			Foundation.Motion.animateIn($('' + activeTab), 'slide-in-right');
		}
	});

	$('.not-found__cta').on('click', function () { history.back(); });

	// Disable the default submit method
	$("form.contact__form").submit(function (e) {
		console.log("Prevent default");
		e.preventDefault();
	});

	$("form.contact__form").validate({
		errorClass: "error",
		errorElement: "span",
		// errorPlacement: function (error, element) {
		// 	console.log ( "Placing error" );
		// 	$(error).text();
		// 	if (element.attr('type') === 'checkbox') {
		// 		$(element).siblings(".error").text($(error).text());
		// 	} else {
		// 		// Append error within associated label
		// 		$(element)
		// 			.closest("form")
		// 				.find("label[for='" + element.attr("name") + "']")
		// 				.find(".newsletter-signup__labeltext")
		// 					.after(error);
		// 		// And make the label text visible
		// 		// $( element )
		// 		// 	.closest( "form" )
		// 		// 		.find( "label[for='" + element.attr( "name" ) + "']" )
		// 		// 			.find('span')
		// 		// 				.css("visibility", "visible");
		// 	}

		// },
		submitHandler: function (form) {
			$(form).find('button.submit').attr("disabled", true).text("Sending...");
			var formData = new FormData(form);
			var jqxhr = $.ajax(
				{
					url: $(form).attr("action"),
					timeout: 10000,
					type: 'POST',
					data: formData,
					processData: false,
					contentType: false
					// ^^^ Important when sending FormData object
				})
				.done(function (data) {
					// Show completion message
					$('div.contact div.contact__thankyou').addClass('active');
				})
				.fail(function (err) {
					console.log("Submission failure");
					$(form).find('button.submit').attr("disabled", false).text("Send");
					alert("Sorry, there was a problem processing your message.\n\nPlease try again, or call us toll-free at 1‑800‑265‑4769");
					console.log(err);
				});
		}
	});


});
// End document.ready


