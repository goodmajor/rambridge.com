import $ from 'jquery';
import validate from 'jquery-validation';
import smoothscroll from 'smoothscroll-polyfill';
import rb from './rb';

 // kick off the polyfill!
smoothscroll.polyfill();

var map, infoWindow, storeData, pos;
function initMap() {
	console.log("Map initialized");
	map = new google.maps.Map(document.getElementById('locator-map'), {
		center: { 
			lat: 45, 
			lng: -81 
		},
		mapTypeId: 'roadmap',
		gestureHandling: 'auto',
		fullscreenControl: false,
		zoom: 5,
		zoomControl: true,
    zoomControlOptions: {
        position: google.maps.ControlPosition.TOP_RIGHT
    },
		mapTypeControl: false,
		streetViewControl: false
	});
	storeData = new google.maps.Data();

	// map.addListener('idle', function() {
		//console.log(map.getCenter());
  // });
}
// !!!IMPORTANT!!! when using WebPack3
window.initMap = initMap;

$(document).ready(function () {

	console.log("Locator Tool loaded...");
	// Google maps setup for Locator tool
	// Default to Englih if not set in browser
	var lang;
	if (document.querySelector('html').lang)
		lang = document.querySelector('html').lang;
	else
		lang = 'en';
	const apiKey = 'AIzaSyDXV_sdo2RNhYCiPIHDtvLlPjhGgVRyEic';
	// Load the GMaps library
	var js_file = document.createElement('script');
	js_file.type = 'text/javascript';
	js_file.src = 'https://maps.googleapis.com/maps/api/js?key=' + apiKey + '&language=' + lang;
	document.getElementsByTagName('head')[0].appendChild(js_file);

	$('#search-here').on('click', function () {
		console.log("Search near here");
		loadDataAndSetBounds(map.getCenter().toJSON());
	});

	$('#locator-main-modal .locator__store-list-expander').on('click', function () {
		// $('').css("max-height", "350px");
		console.log("Expand clicked");
		$('#locator-main-modal .locator__store-list-container').toggleClass("expanded");
		//console.log($(this).toggleClass("expanded"));
	});

	$('.nav__trigger--locator').on('click', function() {
		console.log("Open LOCATOR");
		rb.closeAllModals();
		$('#rb-off-canvas').foundation('close');
		// Try to Geolocate user, decide which modal to show based on success
		//check for Geolocation support
		if (navigator.geolocation) {
			console.log("Requesting browser location");
			navigator.geolocation.getCurrentPosition(geoSuccess, geoError);
		}
		else {
			console.log('Error: Your browser doesn\'t support geolocation.');
			$('#locator-zipcode-modal').foundation('open');
		}
	});

	var storeSearchValidator = $("form#store-search").validate({
		messages: {
			"store-search-address": {
				required: "Please enter your Postal Code / Zip Code",
			}
		},
		invalidHandler: function(event, validator) {
			console.log("Form submit FAILURE");
			console.log(`There are ${validator.numberOfInvalids()} errors`);
		},
		submitHandler: function(form) {
			console.log("Form submit SUCCESS");
			var address = $('#store-search-address').val();	
			var geocoder = new google.maps.Geocoder();
			geocoder.geocode( { 
					'address': address
				}, function(results, status) {
				if (status == google.maps.GeocoderStatus.OK) {
					// console.log(results[0].geometry.location.lat())
					// console.log(results[0].geometry.location.lng())
					pos = {
						lat: results[0].geometry.location.lat(),
						lng: results[0].geometry.location.lng()
					};
					$('#locator-main-modal').foundation('open');
				} else {
					console.log('Geocode failed: ' + status);
					alert('Zipcode / postal code not found. Please check and try again.');
				}
			});
		}	
	});

	// Do things as soon as the Locator modal finishes opening
	$('#locator-main-modal').on('open.zf.reveal', function () {
		initMap();
		loadDataAndSetBounds(pos);
		//google.maps.event.trigger(map, 'resize');
	});

});

function geoSuccess (position) {
	console.log("Geolocation success", position);
	pos = {
		lat: position.coords.latitude,
		lng: position.coords.longitude
	};
	$('#locator-main-modal').foundation('open');
}

function geoError (error) {
	console.log("Error: Geolocation failed", error);
	$('#locator-zipcode-modal').foundation('open');
}

function loadDataAndSetBounds(pos) {
	// Requires a LatLngLiteral
	// Can convert LatLng to LatLingLiteral with LatLng.toJSON() method
	console.log("Loading GeoJSON data");
	console.log(`Requested position is ${pos.lng},${pos.lat}`);
	let geoURL = `/.netlify/functions/location-finder?lon=${pos.lng}&lat=${pos.lat}`;

	console.log("Detaching old GeoJSON data");
	storeData.forEach(function(feature) {
    storeData.remove(feature);
	});

	// Load GeoJSON.
	$.getJSON(geoURL)
	.then(function(stores) {  
		console.log("Received GeoJSON data");

		// Get container for list of stores and empty it
		var storeList = $('div.locator__store-list').empty();

		// We'll set the map bounds sensibly in a moment 
		var bounds = new google.maps.LatLngBounds();

		if (stores.features.length > 0) {
			storeData.addGeoJson(stores);

			var featureIndex = 1;
			storeData.forEach(function (store) {
				var item, itemName, itemDistance, itemAddress, itemPhone, itemWebLink, itemDirectionsLink;

				item = $(`<div id="store-${store.getProperty('id')}" class="grid-x locator__store"></div>`);

				itemName = $(`<div class="cell large-3 locator__store-name">
										${featureIndex++}. ${store.getProperty('name')}
									 </div>`);
									 
				itemDistance = $(`<div class="cell large-1 locator__store-distance show-for-large">
				${ Math.round(store.getProperty('distance'))} km
												</div>`);

				itemAddress = `<div class="cell large-2 locator__store-address">
											 	${store.getProperty('address').replace(/(?:\r\n|\r|\n)/g, '<br>')}
											 </div>`;

				itemPhone = `<div class="cell large-2 locator__store-phone">
											${store.getProperty('phoneNumber')}
											</div>`;

				// TODO: ensure url has http:// or https:// 
				var webURL = store.getProperty('websiteUrl') ? `<a href="${store.getProperty('websiteUrl')}" target="_new" class="cta">Visit Website</a>` : "";
				itemWebLink	= `<div class="cell large-2 small-6 align-self-middle locator__store-cta">${webURL}</div>`;

				var directionsURL = `https://www.google.com/maps/dir/?api=1&destination=${encodeURIComponent(store.getProperty('address'))}`;
				itemDirectionsLink = `<div class="cell large-2 small-6 align-self-middle locator__store-cta"><a href="${directionsURL}" target="_new" class="cta">Get Directions</a></div>`;
															
				item.append([itemName, itemDistance, itemAddress, itemPhone, itemWebLink, itemDirectionsLink]);
				storeList.append(item);

				bounds.extend(store.getGeometry().get());		
			});

			// Ensure the map shows at least a 5km/~3mile radius
			var radius = new google.maps.Circle({
				strokeColor: '#FFF',
				strokeOpacity: 0,
				strokeWeight: 1,
				fillColor: '#FFF',
				fillOpacity: 0,
				map: map,
				radius: 5000,    // metres
				center: bounds.getCenter()
			});
			bounds.union(radius.getBounds());

			// Ensure the map shows the searchers location
			console.log(pos);
			bounds.extend(pos);

			map.fitBounds(bounds);


		} else {
			console.log("No results returned");
			alert("Did not find any stores near you, sorry");
		}


		$('#locator-list .accordion-content').html(storeList);
		storeData.setMap(map);

		storeData.addListener('click', function(event) {
			console.log(`Clicked on ${event.feature.getProperty("id")}`);

			// Part-close the drawer
			$('#locator-main-modal .locator__store-list-container').removeClass("expanded");
			
			var scrollContainer = $('#locator-main-modal .locator__store-list');
			var targetStore = scrollContainer.find(`#store-${event.feature.getProperty("id")}`);

			// For debug only
			targetStore.addClass("locator__store--highlight");
			targetStore.siblings().removeClass("locator__store--highlight");

			// Plain ol' JS scrollIntoView works best here
			targetStore.get(0).scrollIntoView({
				behavior: 'smooth' 
			});
			
		});

	})
	.catch(function(err) {
		console.log(`Loading GeoJSON failed: ${err}`);
		alert("Unable to load location data. Please try again later.");
	});
}

$('form#store-search').submit(function(e){
	e.preventDefault();
});