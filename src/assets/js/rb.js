import $ from 'jquery';

export default {
	isStepValid: isStepValid,
	dumpValues: dumpValues,
	closeAllModals: closeAllModals,
	capFirstLetter: capFirstLetter,
	resetMenu: resetMenu
};

function capFirstLetter(string) {
	return string.charAt(0).toUpperCase() + string.slice(1);
}

function resetMenu() {
	console.log("Resetting menu");
	closeAllModals();
	$('.nav__main-menu-container ul.accordion').foundation('up', $('.accordion-content'));
	$(".menu-scroll-container").scrollTop(0);
}

function closeAllModals() {
	$('.reveal, .rb-reveal').foundation('close');
}

function isStepValid(element) {
	let currentStep = element.attr('data-step');
	let form = element.closest('form');
	// console.log(form);
	console.log(`Is step ${currentStep} valid?`);
	let validity = true;
	element.find('input').each(function (i, el) {
		// console.log(el);
		//$(form).validate().element(el);
		if ( !$(form).validate().element(el)) {
			console.log("Not valid: ", $(el));
			validity = false;	
		}  else {
			console.log("Validated OK", $(el));
		}
	});
	return validity;
}

function dumpValues(form) {
	var allInputs = $(form).find('input, select');	
	var formValues = {};
	allInputs.each(function() {
		if ($(this).attr('type') === "checkbox") {
			formValues[this.name] = $(this).prop('checked');
		} else if ($(this).attr('type') === "radio") {
			if ( $(this).prop('checked') ) {
				formValues[this.name] = this.id;
			}
		} else {
			formValues[this.name] = $(this).val();				
		}
	});
	console.log(formValues);
}