const axios = require("axios")
const qs = require("qs")
const dotenv = require("dotenv")

// If env vars aren't set, look for a .env file
// Should only happen when dev'ing locally
if (! process.env.MC_API_URL) {
	const env = dotenv.config();
	if (env.error) {
		throw env.error
	}
}

exports.handler = function (event, context, callback) {

	// Get env var values defined in our Netlify site UI (or .env file)
	let API_URL = process.env.MC_API_URL;
	if ( !isString(API_URL) ) { 
		callback(null, {
			statusCode: 500,
			body: "API_URL not set"
		});
		return
	}

	const API_QS = qs.parse(event.queryStringParameters)
	let API_TOKEN;
	if ( !isString(API_QS.list) ) { 
		callback(null, {
			statusCode: 500,
			body: "List was not specified"
		});
		return
	} else {
		switch(API_QS.list) {
			case 'rambridge':
				console.log("Subscribe to Rambridge list");
				API_URL = API_URL.replace('LIST_ID', process.env.MC_LIST_RAMBRIDGE);
				API_TOKEN =  process.env.MC_KEY_RAMBRIDGE;
				break;
			case 'liftco':
				console.log("Subscribe to Lift&Co list");
				API_URL = API_URL.replace('LIST_ID', process.env.MC_LIST_LIFTCO);
				API_TOKEN = process.env.MC_KEY_LIFTCO;
				break;
			default: 
				console.log("Didn't recognize list name");
				callback(null, {
					statusCode: 500,
					body: "List name is invalid"
				});
				return
		}
		console.log("Constructed URL is ...", API_URL)
		console.log("API Token is ...", API_TOKEN)
	}

	if ( !isString(API_TOKEN) ) { 
		console.log("API_TOKEN is not a string");
		callback(null, {
			statusCode: 500,
			body: "API_TOKEN not set"
		});
		return
	}

  // Let's log some stuff we already have.
  // console.log("Injecting token to", API_URL);
  // console.log("Logging event.....", event)
  // console.log("Constructed URL is ...", URL)

	const options = {
		timeout: 3000,
		auth: {
			username: "anystring",
			password: API_TOKEN
		}
	}
	// TODO - Handles all verbs
	// TODO - Send error on anything that isn't POST
  if(event.httpMethod == 'GET'){
		axios.get(API_URL, options)
    .then((response) =>
      {
        callback(200, response.data)
      }
    )
    .catch( 
			err => callback(400, "Problem")
		)
	};

	if(event.httpMethod == 'POST'){
    axios.post(API_URL, event.body, options)
    .then( (response) => {
			console.log("Received valid response from upstream");
			callback(null, {
				statusCode: response.status,
				body: JSON.stringify(response.data)
			});
		})
		.catch( (error) => {
			if (error.response) {
				console.log("Received error from upstream");

				// Handle people who are already subscribed
				if ( (error.response.status === 400) && ( error.response.data.title === 'Member Exists')) {
					console.log("Member already subscribed");
					callback(null, {
						statusCode: 200,
						body: JSON.stringify(error.response.data)
					});
				} else {
					// The request was made and the server responded with a status code
					// that falls out of the range of 2xx
					console.log(error.response.data);
					callback(null, {
						statusCode: error.response.status,
						body: JSON.stringify(error.response.data)
					});
				}
			
			} else if (error.request) {
				console.log("No response from upstream");

				// The request was made but no response was received
				// `error.request` is an instance of XMLHttpRequest in the browser and an instance of
				// http.ClientRequest in node.js
				callback(null, {
					statusCode: 500,
					body: JSON.stringify(error.request)
				});
			} else {
				console.log("Error making request");
				// Something happened in setting up the request that triggered an Error
				callback(null, {
					statusCode: 500,
					body: "Unknown internal error"
				});
			}
		})
  };
};

function isString (value) {
	return typeof value === 'string' || value instanceof String;
}