## Netlify Lambda Functions

Functions are built on the dev machine, not as part of build pipeline

Run:- 

`./node_modules/.bin/netlify-lambda serve src/functions/`

### Mailchimp proxy

Requires API_TOKEN, API_URL environment variables to be set