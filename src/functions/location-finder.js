const qs = require('qs');
const util = require('util');
const haversine = require('haversine');
var GeoJSON = require('geojson');
var locations = require('../data/mapLocation.json');

exports.handler = function(event, context, callback) {

	const QS_PARAMS = qs.parse(event.queryStringParameters);

	var passed = {
		latitude: QS_PARAMS.lat,
		longitude: QS_PARAMS.lon,
		maxDistance: QS_PARAMS.distance || 80,
		maxResults: QS_PARAMS.results || 20
	};

	// Make sure query string is meaningful
	if ( typeof passed.latitude === 'undefined' || typeof passed.longitude === 'undefined' ) {
		callback(null, {
			statusCode: 500,
			body: "Invalid coordinates"
		});
	} else {

		// Calculate distance from user for each store
		var results = locations.items.map(function(item, index, arr) {
			var here = { 
				latitude: item.fields.location.lat, 
				longitude: item.fields.location.lon 
			};

			return ({
				id: item.fields.id,
				name: item.fields.name,
				address: item.fields.address,
				phoneNumber: item.fields.phoneNumber,
				websiteUrl: item.fields.websiteUrl,
				distance: haversine(passed, here),
				location: here
			})
		});

		var total, found, returned;

		
		// Sort array of store by distance
		results.sort(function (a, b) {
			return parseFloat(a.distance) - parseFloat(b.distance);
		});
		var total = results.length;

		// Filter by max distance
		results = results.filter(function(item) {
			return item.distance <= passed.maxDistance; 
		});
		found = results.length;

		// Limit number of results returned
		results = results.slice(0, passed.maxResults);
		returned = results.length;

		// Convert sorted/filtered array to GeoJSON format
		var geo = GeoJSON.parse(results, {
			Point: ['location.latitude', 'location.longitude'],
			exclude: 'location'
		});
		
		console.log(`Searched ${total} locations. Found ${found} nearby. Returned ${returned}.`);

		// Send the sorted/filtered/sliced/converted results to the browser
		callback(null, {
			statusCode: 200,
			body: JSON.stringify(geo)
		});

	}
}