module.exports = function (string, options) {
	var bold = (options.hash.hasOwnProperty("bold") ? options.hash.bold : true);
	if(Boolean(string)) {
		var res = string.split("|");
		if (bold) {
			res[0] = "<strong>" + res[0] + "</strong>";
		}
		return res.join(' ');
	} else {
		return "Value unavailable";
	} 
}

