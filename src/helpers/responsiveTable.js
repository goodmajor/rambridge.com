module.exports = function (tableData) {

	if(Boolean(tableData)) {
		// console.log(tableData);

		let rowArray = tableData.map(function(row, index) {
			if (index === 0) {
				// This is the header row
				let cellArray = row.map(function(cell) {
					return "<th>" + cell + "</th>";
				})
				return `<thead><tr>${cellArray.join('')}</tr></thead><tbody>` 
			} else {
				// This is a regular table row
				let cellArray = row.map(function(cell, index) {
					return `<td data-label="${tableData[0][index]}">${cell}</td>`;
				})
				return `<tr>${cellArray.join('')}</tr>` 
			}
			// console.log(cellArray);
		})
		let tableString = `<table>${rowArray.join('')}</tbody></table>`;
		// console.log(tableString);
		return tableString;
	} else {
		return "Table unavailable";
	} 	
}

