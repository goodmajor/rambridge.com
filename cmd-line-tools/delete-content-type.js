
const util = require('util');
const contentful = require('contentful-management');

const SPACE_ID = 'gc78g8r4v8np';
const ENV_ID = 'production';
const ACCESS_TOKEN = 'CFPAT-g0nW41bPH7skNlW8bF14e9jxpJfj3b2K2fgP1qdLuKc';


const client = contentful.createClient({
  accessToken: ACCESS_TOKEN
});

client.getSpace(SPACE_ID)
.then((space) => space.getEnvironment(ENV_ID))
.then((environment) => environment.getEntries({
	content_type: "mapLocation",
	limit: 100
}))
.then((response) => {
	response.items.delayedForEach(function (item) {
		deleteEntry(item.sys.id);
	}, 500);
})
.catch(console.error);

function deleteEntry(id) {
	console.log("Deleting " + id);
	client.getSpace(SPACE_ID)
	.then((space) => space.getEnvironment(ENV_ID))
	.then((environment) => environment.getEntry(id))
	.then((entry) => entry.unpublish())
	.then((entry) => entry.delete())
	.catch(console.error);
}

Array.prototype.delayedForEach = function(callback, timeout, thisArg){
  var i = 0,
    l = this.length,
    self = this,
    caller = function(){
      callback.call(thisArg || self, self[i], i, self);
      (++i < l) && setTimeout(caller, timeout);
    };
  caller();
};