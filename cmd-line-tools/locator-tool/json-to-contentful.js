const fs = require('fs');
const util = require('util');
const contentful = require('contentful-management');
const merge = require('lodash.merge');

const SPACE_ID = 'gc78g8r4v8np';
const ENV_ID = 'production';
const ACCESS_TOKEN = 'CFPAT-g0nW41bPH7skNlW8bF14e9jxpJfj3b2K2fgP1qdLuKc';
const jsonFilePath = 'customer-list+geo.json';


const client = contentful.createClient({
  accessToken: ACCESS_TOKEN
});

fs.readFile(jsonFilePath, function (err, data) {
	if (!err) {
	
		var stores = JSON.parse(data);
		stores.delayedForEach(function(elem, index, array){ 
			//console.log(elem);
			updateInContentful(elem); 
		}, 500);

	} else {
		console.log("Couldn't read file, aborting");
	}
});


function updateInContentful(store) {

	var csvEntry = {
		fields: {
			id: {
				'en-US': parseInt(store.id)
			},
			location: {
				"en-US": {
					"lon": store.lng,
					"lat": store.lat
				}
			},
			name: {
				'en-US': toUpper(store.customer_name)
			},
			address: {
				'en-US': toUpper(store.street_address) + '\n' +
								toUpper(store.city) + '\n' +
								store.state + '\n' +
								toUpper(store.country) + '\n' +
								store.zipcode
			},
			phoneNumber: {
				'en-US': ""
			},
			websiteUrl: {
				'en-US': ""
			}
		}};
	console.log(csvEntry);

	// client.getSpace(SPACE_ID)
	// .then((space) => space.getEnvironment(ENV_ID))
	// .then((environment) => environment.getEntries({
	// 	content_type: 'mapLocation',
	// 	'fields.id': store.id
	// }))
	// .then((response) => {
	// 	console.log(response);
	// 	var dlEntry = response.items[0] || {};
	// 	merge(dlEntry, csvEntry);
	// 	//entry.fields.name = { "en-US": "Shortened Store Name" };
	// 	return response.update()
	// })
//	.then((entry) => entry.publish())
	// .catch(console.error)	

	client.getSpace(SPACE_ID)
		.then((space) => space.getEnvironment(ENV_ID))
		.then((environment) => environment.createEntry('mapLocation', csvEntry))
		.then((entry) => entry.publish())
		.then((entry) => console.log(entry))
		.catch(console.error)
}


Array.prototype.delayedForEach = function(callback, timeout, thisArg){
  var i = 0,
    l = this.length,
    self = this,
    caller = function(){
      callback.call(thisArg || self, self[i], i, self);
      (++i < l) && setTimeout(caller, timeout);
    };
  caller();
};

function toUpper(str) {
	return str
			.trim()
			.toLowerCase()
			.split(' ')
			.map(function(word) {
					// console.log("First capital letter: "+word[0]);
					// console.log("remaining letters: "+ word.substr(1));
					if (word.length) {
						return word[0].toUpperCase() + word.substr(1);
					} else {
						return "";
					}
			})
			.join(' ');
	 }
