const util = require('util');
const fs = require('fs');

const googleMapsClient = require('@google/maps').createClient({
	key: 'AIzaSyATCUu7EKlSYsDS0mVH7eSDhT3EyxsKgZY',
	Promise: Promise
});

DO_NOT_UPDATE = true;

const jsonFilePath = 'customer-list+geo.json';
const jsonOutputFilePath = 'customer-list+geo.json';

fs.readFile(jsonFilePath, function (err, data) {
	if (!err) {
		let customers = JSON.parse(data);
		var promises = customers.map(function (customer, index) {

			//console.log(customer.customer_name + ", " + customer.street_address + ", " + customer.city + ", " + customer.state + ", " + customer.country + ", " + customer.zipcode);

			if ((typeof(customer.lat) == 'number') && (typeof(customer.lng) == 'number') && DO_NOT_UPDATE) {
				process.stdout.write("-");

				//process.stdout.write("\nSkipping " + customer.customer_name + ", already have coords");
				return customer;
			}

			if (index < 7000) {
				return googleMapsClient.geocode({
					address: customer.street_address + ", " + customer.city + ", " + customer.state + ", " + customer.country + ", " + customer.zipcode
				}).asPromise()
					.then((response) => {
						process.stdout.write("G");
						try{
							let coords = response.json.results[0].geometry.location;
							customer.lat = coords.lat;
							customer.lng = coords.lng;
						} catch (err){
							//console.log(util.inspect(response, true, 3));
							process.stdout.write("\nUnable to get coords for " + customer.customer_name);
							customer.lat = "";
							customer.lng = "";
						}
						return customer;
					})
					.catch((err) => {
						// Let's deal with geocode attempts that failed
						console.log(err);
						customer.lat = "";
						customer.lng = "";
						return customer;
					});
			} else {
				process.stdout.write('|');
				return customer;
			}
			
		});
		Promise.all(promises)
			.then(function (results) {
				process.stdout.write("\nPromises all resolved happily");
				fs.writeFile(jsonOutputFilePath, JSON.stringify(results, null, 2), function (err) {
					if (!err) {
						process.stdout.write("\nThe file was saved!\n");
					} else {
						console.log(err);
					}
				});
			});
	} else {
		console.log("Couldn't read file, aborting");
	}
});
