const csvtojson = require('csvtojson');
const fs = require('fs');

const csvFilePath = 'customer-list.csv';
const jsonFilePath = 'customer-list.json';

csvtojson()
.fromFile(csvFilePath)
.then((jsonObj)=>{
	// console.log(jsonObj.length);
	fs.writeFileSync(jsonFilePath, JSON.stringify(jsonObj));
});