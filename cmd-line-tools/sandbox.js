const fs = require('fs');
const util = require('util');
const contentful = require('contentful');
const merge = require('lodash.merge');

const contentfulOptions = {
	space: '09thoqeer93l',
	environment: 'master-2019-10-21',
	accessToken: 'iaCGhD2w8uCV28T3FroNaCH0ZJvnZBhBw9vZQNx32Kk'
};

const client = contentful.createClient(contentfulOptions);


var content;
// First I want to read the file
fs.readFile('locator-tool/customer-list.json', function (err, data) {
    if (err) {
        throw err;
    }
    content = JSON.parse(data)// Invoke the next step here however you like
    console.log(`Customer list contains ${content.length} records`);   // Put all of the code here (not the best solution)
});

fs.readFile('locator-tool/customer-list+geo.json', function (err, data) {
	if (err) {
			throw err;
	}
	content = JSON.parse(data)// Invoke the next step here however you like
	console.log(`Customer list inc GEO contains ${content.length} records`);   // Put all of the code here (not the best solution)
});

fs.readFile('import-export/contentful-export-gc78g8r4v8np-production-2019-10-21T08-57-12.json', function (err, data) {
	if (err) {
			throw err;
	}
	content = JSON.parse(data)// Invoke the next step here however you like
	filteredContent = content.entries.filter(function(entry) {
		return entry.sys.contentType.sys.id === "mapLocation";
	});
	console.log(`Export from GM space contains ${filteredContent.length} records`);   // Put all of the code here (not the best solution)
});

client.getEntries({limit: 1, content_type: "mapLocation"})
.then(function (results) {
	console.log(`Contentful contains ${results.total} records`);
});



// 		environment.getEntries({
// 		content_type: "mapLocation",
// 		limit: 100
// 	})


// })
// .finally(function() {
// 	console.log("And we're done");
// });




// ((num_entries / 1000) + 1).times do |i|
//   query[:limit] = 1000
//   query[:skip] = i * 1000
//   page = client.entries(query)
//   page.each { |entry| all_entries << entry }
// end 



// client.getSpace(SPACE_ID)
// .then((space) => space.getEnvironment(ENV_ID))
//   .then((environment) => environment.getEntry('2HiVsqg2jNv60vfxfz453c'))
//   .then((entry) => {
//     entry.fields.name = { "en-US": "Shortened Store Name" };
//     return entry.update()
// 	})
// 	.then((entry) => entry.publish())
// 	.catch(console.error)
	



Array.prototype.delayedForEach = function(callback, timeout, thisArg){
  var i = 0,
    l = this.length,
    self = this,
    caller = function(){
      callback.call(thisArg || self, self[i], i, self);
      (++i < l) && setTimeout(caller, timeout);
    };
  caller();
};

function toUpper(str) {
	return str
			.trim()
			.toLowerCase()
			.split(' ')
			.map(function(word) {
					// console.log("First capital letter: "+word[0]);
					// console.log("remaining letters: "+ word.substr(1));
					if (word.length) {
						return word[0].toUpperCase() + word.substr(1);
					} else {
						return "";
					}
			})
			.join(' ');
	 }
