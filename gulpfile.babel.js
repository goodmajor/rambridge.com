'use strict';

import plugins from 'gulp-load-plugins';
import yargs from 'yargs';
import util from 'util';
import browser from 'browser-sync';
import gulp from 'gulp';
import panini from 'panini';
import rimraf from 'rimraf';
import sherpa from 'style-sherpa';
import yaml from 'js-yaml';
import fs from 'fs';
import webpackStream from 'webpack-stream';
import webpack2 from 'webpack';
import named from 'vinyl-named';
import uncss from 'uncss';
import autoprefixer from 'autoprefixer';
import * as contentful from 'contentful';
import metalsmith from 'gulp-metalsmith';
import layouts from 'metalsmith-layouts';
import debug from 'metalsmith-debug';
import discoverPartials from 'metalsmith-discover-partials';
import discoverHelpers from 'metalsmith-discover-helpers';
import metadata from 'metalsmith-metadata';
import helpers from 'handlebars-helpers';
import { exec } from 'child_process';
import dotenv from 'dotenv';

import imagemin from 'gulp-imagemin';

import dartSass from 'sass';
import gulpSass from 'gulp-sass';
const sass = gulpSass(dartSass);

// Load all Gulp plugins into one variable
const $ = plugins();

// If env vars aren't set, look for a .env file
// Should only happen when dev'ing locally
if (! process.env.cfSpace) {
	const env = dotenv.config();
	if (env.error) {
		throw env.error
	}
}

const contentfulOptions = {
	space: process.env.cfSpace,
	environment: process.env.cfEnvironment,
	accessToken: process.env.cfAccessToken
};


// Check for --production flag
const PRODUCTION = !!(yargs.argv.production);

// Load settings from settings.yml
const { COMPATIBILITY, PORT, UNCSS_OPTIONS, PATHS, CONTENTFUL } = loadConfig();

function loadConfig() {
	let ymlFile = fs.readFileSync('config.yml', 'utf8');
	return yaml.load(ymlFile);
}

function metalsmithJson () {
  return gulp.src('src/contentful/*.json')
    .pipe(metalsmith({
      json: true,
      use: [
				// debug( {metalsmith: {
				// 	log: "first debug",      // any comment you like
				// 	metadata: true,         // default: true
				// 	source: false,           // default: true
				// 	destination: false,      // default: true
				// 	files: true             // default: true
				// }}),		
				metadata({
					"files": { 
						menu: 'src/data/menu.json',
						supportDocs: 'src/data/supportDocs.json',
						supportVideos: 'src/data/supportVideos.json' 
					},
					"config": { isExternalSrc: true }
				}),
				discoverPartials({
					directory: 'src/partials',
					pattern: /\.html$/
				}),
				discoverHelpers({
					directory: 'src/helpers'
				}),
				layouts({
					engine: 'handlebars',
					directory: 'src/metalsmith-templates'
				})
			]
		}))
	.pipe(gulp.dest(PATHS.dist));
}

// Used to get Contentful data that's used by the Foundation/Panini-generated 
// pages. Yes, we're downloading data from Contentful two different ways, for
// two different purposes
function getContentfulDataForLambdas(cb) {
	var client = contentful.createClient(contentfulOptions);

	//const content_types = CONTENTFUL.content_type;

	let promiseMap = getAllRecords("mapLocation", "src/data/mapLocation.json");

	// Get all records from Contentful, in batches 
	// Note: Contentful only allows 1000 records per request
	function getAllRecords(contentType, filename) {

		var total;
		var batchSize = 100;
		var promiseSet = [];
		var resultSet = [];
		client.getEntries({limit: 1, content_type: contentType})
		.then(function (results) {
			// console.log(`Contentful contains ${results.total} ${contentType} records`)
			total = results.total;
		})
		.catch(function(err) {
			console.log("Got an error ", err);
			cb(new Error(err));
		})
		.then(function() {
			for (var i = 0; i < (total/batchSize); i++) { 		
				promiseSet.push(
					client.getEntries({
						content_type: contentType	,
						limit: 100,
						skip: i*batchSize
					})
					.then(function(results) {
						return new Promise((resolve, reject) => {
							results.items.forEach(function(item) {
								resultSet.push(item);
							});
							resolve();				
						})
					})
				);		//console.log(promiseSet);
			}

			Promise.all(promiseSet)
			.then(function(response) {
				console.log(`Got ${resultSet.length} ${contentType} records`);
				resultSet = { items: resultSet };
				fs.writeFile(filename, JSON.stringify(resultSet), function(err) {
					if(err) {
						return console.log(err);
					}
					cb();
					// console.log("The file was saved!");
				}); 
			})
		})
	};
}

// Used to get content from Contentful API, for Metalsmith-generated pages only
function getContentfulDataForMetalsmith (cb) {
	var client = contentful.createClient(contentfulOptions);
	console.log(`Using space ${contentfulOptions.space}, environment ${contentfulOptions.environment}`);

  let promiseProducts = client.getEntries({	
		content_type: 'product',
		limit: 500
    //order: 'fields.order'
	})
	.then(function (entries) {
		var pages = preparePages(entries, "product-details");
		console.log(`Got ${Object.keys(pages).length} Products`);		
		fs.writeFileSync('src/contentful/products.json', JSON.stringify(pages, null, 2));
	})
	.catch(function(err) {
		console.log("Got an error", err);
		cb(new Error(err));
	});

	let promiseCategories = client.getEntries({	
		content_type: 'product',
		include: 2,
		limit: 500
		//order: 'fields.order'
	})
	.then(function (entries) {
		var pages = prepareCategories(entries, "product-listing");
		console.log(`Got ${Object.keys(pages).length} Categories`);
		fs.writeFileSync('src/contentful/categories.json', JSON.stringify(pages, null, 2));
	})
	.catch(function(err) {
		console.log("Got an error", err);
		cb(new Error(err));
	});

	let promiseMenu = client.getEntries({	
		content_type: "menuHeading",
		include: 3,
		order: 'fields.order'
	})
	.then(function (entries) {
		console.log(`Got ${Object.keys(entries).length} Menu Headings`);	
		fs.writeFileSync('src/data/menu.json', JSON.stringify(entries, null, 3));
	})
	.catch(function(err) {
		console.log("Got an error", err);
		cb(new Error(err));
	});
	
	let promiseCarouselSimple = client.getEntries({	
		content_type: "carouselSimple",
		include: 4
	})
	.then(function (entries) {
		var crItems = entries.items.reduce(function(acc, item) {
			return acc + item.fields.items.length;
		}, 0);
		console.log(`Got ${entries.items.length} Simple Carousels containing ${crItems} total items`);
		fs.writeFileSync('src/data/carouselSimple.json', JSON.stringify(entries, null, 3));
	})
	.catch(function(err) {
		console.log("Got an error", err);
		cb(new Error(err));
	});

	let promiseCarouselFancy = client.getEntries({	
		content_type: "carouselFancy",
		include: 4
	})
	.then(function (entries) {
		var crItems = entries.items.reduce(function(acc, item) {
			return acc + item.fields.items.length;
		}, 0);
		console.log(`Got ${entries.items.length} Fancy Carousels containing ${crItems} total items`);	
		fs.writeFileSync('src/data/carouselFancy.json', JSON.stringify(entries, null, 3));
	})
	.catch(function(err) {
		console.log("Got an error", err);
		cb(new Error(err));
	});

	let promiseSupportDocs = client.getEntries({	
		content_type: "supportDocumentGroup",
		include: 3,
		order: 'fields.groupHeading'
	})
	.then(function (entries) {
		var sdItems = entries.items.reduce(function(acc, item) {
			return acc + item.fields.items.length;
		}, 0);
		console.log(`Got ${entries.items.length} Support Doc Groups containing ${sdItems} total documents`);
		fs.writeFileSync('src/data/supportDocs.json', JSON.stringify(entries, null, 3));
	})
	.catch(function(err) {
		console.log("Got an error", err);
		cb(new Error(err));
	});

	let promiseSupportVideos = client.getEntries({	
		content_type: "supportVideoGroup",
		include: 3,
		order: 'fields.groupHeading'
	})
	.then(function (entries) {
		var sdItems = entries.items.reduce(function(acc, item) {
			return acc + item.fields.items.length;
		}, 0);
		console.log(`Got ${entries.items.length} Support Video Groups containing ${sdItems} total videos`);
		fs.writeFileSync('src/data/supportVideos.json', JSON.stringify(entries, null, 3));
	})
	.catch(function(err) {
		console.log("Got an error", err);
		cb(new Error(err));
	});

	let promiseCEADownloadsMedia = client.getEntries({	
		content_type: "ceaDownloadsMedia",
		order: 'sys.updatedAt'
	})
	.then(function (entries) {
		console.log(`Got ${entries.items.length} CEA Download Links`);
		fs.writeFileSync('src/data/ceaDownloadsMedia.json', JSON.stringify(entries, null, 3));
	})
	.catch(function(err) {
		console.log("Got an error", err);
		cb(new Error(err));
	});

	let promiseCEANews = client.getEntries({	
		content_type: "ceaNews",
		// include: 3,
		order: '-fields.publishedDate'
	})
	.then(function (entries) {
		console.log(`Got ${entries.items.length} CEA News items`);
		fs.writeFileSync('src/data/ceaNews.json', JSON.stringify(entries, null, 3));
	})
	.catch(function(err) {
		console.log("Got an error", err);
		cb(new Error(err));
	});


	let promiseImageCarousel = client.getEntries({	
		content_type: "imageCarousel"
	})
	.then(function (entries) {
		console.log(`Got ${entries.items.length} Image Carousels`);

		fs.writeFileSync('src/data/imageCarousel.json', JSON.stringify(entries, null, 3));
	})
	.catch(function(err) {
		console.log("Got an error", err);
		cb(new Error(err));
	});


	let promiseContentSnippet = client.getEntries({	
		content_type: "contentSnippet"
		// include: 3,
		// order: '-fields.publishedDate'
	})
	.then(function (entries) {
		console.log(`Got ${entries.items.length} Content Snippets`);
		// console.log(util.inspect(entries.items, null, 5))
		let snippets = {};
		entries.items.map(item => {
			snippets[item.fields.key] = item.fields.content
		})
		// console.log(util.inspect(snippets, null, 5))
		fs.writeFileSync('src/data/snippets.json', JSON.stringify(snippets, null, 3));
	})
	.catch(function(err) {
		console.log("Got an error", err);
		cb(new Error(err));
	});

	Promise.all([promiseContentSnippet, promiseMenu, promiseProducts, promiseCategories, promiseCarouselSimple, promiseCarouselFancy, promiseSupportDocs, promiseSupportVideos])
	.then(function() {
		cb();
	})

	function preparePages(entries, layout) {
		return entries.items.reduce(function (pages, item) {
			pages[item.fields.slug + ".html"] = {
				title: item.fields.name + " at Rambridge Wholesale Supply",
				// order: item.fields.order,
				layout: layout + '.hbs',
				contents: '',
				data: item.fields
			};
			return pages;
		}, {});
	}

	function prepareCategories(entries, layout) {		
		return entries.items.reduce(function (pages, item, index, array) {
			// console.log(item.fields.name);
			if (! pages.hasOwnProperty([item.fields.category.fields.slug + ".html"])) {
				pages[item.fields.category.fields.slug + ".html"] = {
					name: item.fields.category.fields.name,
					title: item.fields.category.fields.name + " at Rambridge Wholesale Supply",
					parent: item.fields.category.fields.parent.fields.name,
					layout: layout + '.hbs',
					contents: '',
				};
			}
			if (typeof(pages[item.fields.category.fields.slug + ".html"].data) === 'undefined') {
				pages[item.fields.category.fields.slug + ".html"].data = [];
			}
			pages[item.fields.category.fields.slug + ".html"].data.push(item);
			return pages;
		}, {});
	}
}

// Build the "dist" folder by running all of the below tasks
// Sass must be run later so UnCSS can search for used classes in the others assets.
gulp.task('build',
	gulp.series(
		clean, 
		gulp.parallel(getContentfulDataForMetalsmith, getContentfulDataForLambdas),
		gulp.parallel(pages, metalsmithJson, javascript, images, copy, favicon, lambdas), 
		scss
		// styleGuide
	)
);

// Build the site, run the server, and watch for file changes
gulp.task('default',
	gulp.series('build', server, watch));

// Get data from Contentful
gulp.task('lambdas', lambdas);
gulp.task('data', gulp.parallel(getContentfulDataForMetalsmith, getContentfulDataForLambdas));
gulp.task('metalsmith', metalsmithJson);

// Delete the "dist" folder
// This happens every time a build starts
function clean(done) {
	rimraf(PATHS.dist + " " + PATHS.functions_dist, done);
}

// Copy files out of the assets folder
// This task skips over the "img", "js", and "scss" folders, which are parsed separately
function copy() {
	return gulp.src(PATHS.assets)
		.pipe(gulp.dest(PATHS.dist + '/assets'));
}

function favicon() {
	return gulp.src(PATHS.favicon)
	.pipe(gulp.dest(PATHS.dist));	
}

// Copy page templates into finished HTML files
function pages() {
	return gulp.src('src/pages/**/*.{html,hbs,handlebars}')
		.pipe(panini({
			root: 'src/pages/',
			layouts: 'src/layouts/',
			partials: 'src/partials/',
			data: 'src/data/',
			helpers: 'src/helpers/'
		}))
		// .pipe(i18n({
		// 	locales: ['en-CA', 'fr-CA'],
		// 	localeDir: 'src/locales',
		// 	delimeters: ['!{', '}!'],
		// 	ignoreErrors: false,
		// 	schema: 'directory'
		// }))
		.pipe(gulp.dest(PATHS.dist));
}

// Load updated HTML templates and partials into Panini
function resetPages(done) {
	panini.refresh();
	done();
}

// Generate a style guide from the Markdown content and HTML template in styleguide/
function styleGuide(done) {
	sherpa('src/styleguide/index.md', {
		output: PATHS.dist + '/styleguide.html',
		template: 'src/styleguide/template.html'
	}, done);
}

// Compile Sass into CSS
// In production, the CSS is compressed
function scss() {

	const postCssPlugins = [
		// Autoprefixer
		autoprefixer({ browsers: COMPATIBILITY }),

		// UnCSS - Uncomment to remove unused styles in production
		// PRODUCTION && uncss.postcssPlugin(UNCSS_OPTIONS),
	].filter(Boolean);

	return gulp.src('src/assets/scss/app.scss')
		.pipe($.sourcemaps.init())
		.pipe(sass({ includePaths: PATHS.sass})
		.on('error', sass.logError))
		.pipe($.postcss(postCssPlugins))
		.pipe($.if(PRODUCTION, $.cleanCss({ compatibility: 'ie9' })))
		.pipe($.if(!PRODUCTION, $.sourcemaps.write()))
		.pipe(gulp.dest(PATHS.dist + '/assets/css'))
		.pipe(browser.reload({ stream: true }));
}

let webpackConfig = {
	mode: (PRODUCTION ? 'production' : 'development'),
	module: {
		rules: [
			{
				test: /\.js$/,
				use: {
					loader: 'babel-loader',
					options: {
						presets: ["@babel/preset-env"],
						compact: false
					}
				}
			}
		]
	},
	devtool: !PRODUCTION && 'source-map'
}

// Combine JavaScript into one file
// In production, the file is minified
function javascript() {
	return gulp.src(PATHS.entries)
		.pipe(named())
		.pipe($.sourcemaps.init())
		.pipe(webpackStream(webpackConfig, webpack2))
		.pipe($.if(PRODUCTION, $.uglify({
				compress: {
					drop_console: true
				}
			})
			.on('error', e => { console.log(e); })
		))
		.pipe($.if(!PRODUCTION, $.sourcemaps.write()))
		.pipe(gulp.dest(PATHS.dist + '/assets/js'));
}

// Build netlify functions
function lambdas(cb) {
	exec('./node_modules/.bin/netlify-lambda build src/functions', function (err, stdout, stderr) {
		console.log(stdout);
		console.log(stderr);
		cb(err);
	});
};


// Copy images to the "dist" folder
// In production, the images are compressed
function images() {

	return gulp.src('src/assets/img/**/*')
		.pipe(imagemin())
		.pipe(gulp.dest(PATHS.dist + '/assets/img'));
}

// Start a server with BrowserSync to preview the site in
function server(done) {
	browser.init({
		server: PATHS.dist, 
		port: PORT,
		https: true
	}, done);
}

// Reload the browser with BrowserSync
function reload(done) {
	browser.reload();
	done();
}

// Watch for changes to static assets, pages, Sass, and JavaScript
function watch() {
	gulp.watch(PATHS.assets, copy);
	gulp.watch(PATHS.favicon, favicon);
	gulp.watch('src/{metalsmith-templates,data,contentful}/**/*').on('all', gulp.series(metalsmithJson, browser.reload));
	gulp.watch('src/pages/**/*.html').on('all', gulp.series(pages, browser.reload));
	gulp.watch('src/{layouts,partials}/**/*.html').on('all', gulp.series(resetPages, pages, browser.reload));
	gulp.watch('src/data/**/*.{js,json,yml}').on('all', gulp.series(resetPages, pages, browser.reload));
	gulp.watch('src/helpers/**/*.js').on('all', gulp.series(resetPages, pages, metalsmithJson, browser.reload));
	gulp.watch('src/assets/scss/**/*.scss').on('all', scss);
	gulp.watch('src/assets/js/**/*.js').on('all', gulp.series(javascript, browser.reload));
	gulp.watch('src/assets/img/**/*').on('all', gulp.series(images, browser.reload));
	gulp.watch('src/styleguide/**').on('all', gulp.series(styleGuide, browser.reload));
	gulp.watch('src/functions/**').on('all', lambdas);
}
